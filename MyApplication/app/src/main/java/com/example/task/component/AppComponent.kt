package com.example.task.component

import com.example.task.ui.list.MovieViewModel
import com.example.task.request.MovieServiceModule

import javax.inject.Singleton

import dagger.Component

@Component(modules = [MovieServiceModule::class])
@Singleton
interface AppComponent {
    fun inject(movieViewModel: MovieViewModel)
}
