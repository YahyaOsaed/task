package com.example.task.app

import android.app.Application
import android.content.Context

import com.example.task.component.AppComponent

import com.example.task.component.DaggerAppComponent
import com.example.task.request.MovieServiceModule

import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers

class AppController : Application() {
    private var scheduler: Scheduler? = null

    fun subscribeScheduler(): Scheduler? {
        if (scheduler == null) {
            scheduler = Schedulers.io()
        }

        return scheduler
    }

    override fun onCreate() {
        super.onCreate()
        appComponent = buildAppComponent()
    }

    fun buildAppComponent(): AppComponent {
        return DaggerAppComponent.builder().movieServiceModule(MovieServiceModule()).build()
    }

    companion object {
        var appComponent: AppComponent? = null
            private set

        private operator fun get(context: Context): AppController {
            return context.applicationContext as AppController
        }

        fun create(context: Context): AppController {
            return AppController[context]
        }
    }

}
