package com.example.task.ui

import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.LinearLayoutManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders

import com.example.R
import com.example.databinding.ActivityMainBinding
import com.example.task.ui.list.MovieAdapter
import com.example.task.ui.list.MovieDetail
import com.example.task.ui.list.MovieViewModel

class MainActivity : AppCompatActivity() {

    private var movieViewModel: MovieViewModel? = null
    var mainActivityBinding: ActivityMainBinding? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initBinding()
        movieViewModel!!.getMovies()
        setRecycler(mainActivityBinding!!.recViewMovies)
        val model = ViewModelProviders.of(this@MainActivity).get(MovieViewModel::class.java)
        model.getMovies().observe(this, Observer<List<MovieDetail>> { moviesList : List<MovieDetail>? ->
            (mainActivityBinding!!.recViewMovies.adapter as MovieAdapter).setMovieList(moviesList!!)
        })
    }

    private fun initBinding() {
        mainActivityBinding = DataBindingUtil.setContentView(this, R.layout.activity_main) //layoutu set ediyor.
        movieViewModel = MovieViewModel(this.application)
        mainActivityBinding!!.movieViewModel = movieViewModel
    }

    private fun setRecycler(listmovies: RecyclerView) {
        val movieAdapter = MovieAdapter(this)
        listmovies.adapter = movieAdapter
        listmovies.layoutManager = LinearLayoutManager(this)
    }

    override fun onDestroy() {
        movieViewModel!!.reset()
        super.onDestroy()
    }
}
