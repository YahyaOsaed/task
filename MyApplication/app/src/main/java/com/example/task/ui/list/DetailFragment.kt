package com.example.task.ui.list

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders


class DetailFragment : Fragment() {

    private lateinit var model: MovieViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        model = activity?.run {
            ViewModelProviders.of(this)[MovieViewModel::class.java]
        } ?: throw Exception("Invalid Activity")
        model.movieList.observe(this, Observer<List<MovieDetail>> { item ->
            // Update the UI
        })
    }
}