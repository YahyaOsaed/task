package com.example.task.ui.list

import android.app.Application
import android.content.Context
import android.view.View
import androidx.databinding.ObservableInt
import androidx.lifecycle.*
import com.example.task.app.AppController
import com.example.task.request.MovieService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import java.util.*
import javax.inject.Inject


class MovieViewModel constructor (application: Application) : AndroidViewModel(application) {

    private val userRecycler: ObservableInt = ObservableInt(View.VISIBLE)
    var movieList: MutableLiveData<List<MovieDetail>> = MutableLiveData()
    private var compositeDisposable: CompositeDisposable? = CompositeDisposable()
    var page = 1

    @Inject
    lateinit var movieService: MovieService

    fun getMovies(): LiveData<List<MovieDetail>>{
        val appController = AppController.create(getApplication())
        AppController.appComponent?.inject(this)
        val disposable = movieService.fetchMovies("discover/movie?api_key=52e737d96b5fb2c858aea839a1882eb8&page=$page")
                .subscribeOn(appController.subscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ movieModal ->
                    println(movieModal.movieList?.size)
                    updateMovieList(movieModal.movieList!!)
                    userRecycler.set(View.VISIBLE)
                }, {
                    userRecycler.set(View.GONE)
                })

        compositeDisposable!!.add(disposable)
        return movieList
    }

    private fun updateMovieList(movies: List<MovieDetail>) {
        page++
        movieList.value = movies
    }

    fun handleRealmDatabaseEntry(movies: List<MovieDetail>) {
        /*  for (MovieDetail movie : movies)
            addMovieToRealm(movie)*/
    }

    fun addMovieToRealm(movie: MovieDetail) {
        /*Realm.init(context!!)
        val mRealm = Realm.getDefaultInstance()
        mRealm.beginTransaction()
        val movieRealmObject = mRealm.createObject(MovieRealmObject::class.java)
        movieRealmObject.title = movie.title
        mRealm.commitTransaction()*/
    }

    fun reset() {
        if (compositeDisposable != null && !compositeDisposable!!.isDisposed) {
            compositeDisposable!!.dispose()
        }
        compositeDisposable = null
        movieList = MutableLiveData()
    }
}
