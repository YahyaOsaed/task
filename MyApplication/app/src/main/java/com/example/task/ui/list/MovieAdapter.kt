package com.example.task.ui.list

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.lifecycle.MutableLiveData
import com.bumptech.glide.Glide

import com.example.R
import com.example.task.ui.MainActivity

class MovieAdapter internal constructor() : RecyclerView.Adapter<MovieAdapter.MovieAdapterViewHolder>() {

    private var movieList: List<MovieDetail>? = ArrayList()
    constructor(movieList: List<MovieDetail>?) : this() {
        this.movieList = movieList
    }

    constructor(movieList: MutableLiveData<List<MovieDetail>>) : this() {
        this.movieList = movieList.value
    }

    private lateinit var mainActivity: MainActivity

    constructor(mainActivity: MainActivity) : this(){
        this.mainActivity = mainActivity
    }

    init {
        this.movieList = ArrayList()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieAdapterViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.movie_item, parent, false)
        return MovieAdapterViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MovieAdapterViewHolder, position: Int) {
        val movieDetail = movieList!![position]
        holder.bindData(movieDetail, mainActivity)
    }

    override fun getItemCount(): Int {
        return movieList!!.size
    }

    internal fun setMovieList(movieList: List<MovieDetail>) {
        this.movieList = movieList
        notifyDataSetChanged()
    }

    class MovieAdapterViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var movieTitle: TextView
        var movieImage: ImageView
        var movieRating: RatingBar

        init {
            movieTitle = view.findViewById(R.id.tvMovieNameDynamic)
            movieImage = view.findViewById(R.id.ivMoviePicture)
            movieRating = view.findViewById(R.id.rbMovieRating)
        }

        fun bindData(movieDetail: MovieDetail, mainActivity: MainActivity) {
            movieTitle.text = movieDetail.title
            movieRating.rating = movieDetail.voteAverage?.toFloat()!!
            Glide.with(mainActivity).load("https://image.tmdb.org/t/p/original/${movieDetail.backdropPath}").into(movieImage);
        }

    }

}
