package com.example.task.request

import com.example.task.ui.list.MovieModal

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Url

interface MovieService {
    @GET
    fun fetchMovies(@Url url: String): Observable<MovieModal>

}
